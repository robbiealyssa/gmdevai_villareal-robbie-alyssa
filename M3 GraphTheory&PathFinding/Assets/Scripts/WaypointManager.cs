﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Link {
    public enum direction {UNI, BI}; //unidirectional, bidirectional
    public GameObject node1;
    public GameObject node2;
    public direction dir;
}

public class WaypointManager : MonoBehaviour {
    private const int V = 1;
    public GameObject[] waypoints;
    public Link[] links;
    public Graph graph = new Graph();

    void Start(){

        if(waypoints.Length > 0){

            foreach (GameObject wp in waypoints){
                graph.AddNode(wp);
            }

            foreach (Link V in links) {
                graph.AddEdge(V.node1, V.node2);
                if(V.dir == Link.direction.BI){
                    graph.AddEdge(V.node2, V.node1);
                }

            }
            
        }

    }

    
    void Update() {
        graph.debugDraw();
    }
}
