﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPoints : MonoBehaviour {
   Transform goal;
   public float speed = 8f;
   public float accuracy = 1f;
   float rotationSpeed = 2f;
   int currentWaypointIndex = 0;
   public GameObject wpManager;
   GameObject[] wps;
   GameObject currentNode;
   Graph graph;


    void Start() {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0]; 
    }

    void LateUpdate() { 
        //checks if graph has waypoints
        if(graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength()) {
            return;
        } 
        
        //current nearest node
        currentNode = graph.getPathPoint(currentWaypointIndex);

        //measure if close enough to current waypoint, if close move to next waypoint
        //closest waypoint and current position
        if(Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, transform.transform.position) < accuracy){
            currentWaypointIndex++;
        }

        if(currentWaypointIndex < graph.getPathLength()){
            goal = graph.getPathPoint(currentWaypointIndex).transform;

            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                        Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    // 
    public void goToHelipad(){
         
         //current location to the location you want to go
         graph.AStar(currentNode, wps[0]);
         //reset index
         currentWaypointIndex = 0;
    }
   
    public void goToTwinMountains(){
        graph.AStar(currentNode, wps[0]);
        currentWaypointIndex = 0; 
    }
    public void goToBarracks(){
        graph.AStar(currentNode, wps[1]);
        currentWaypointIndex = 0; 
    }
    public void goToCommandCenter(){
        graph.AStar(currentNode, wps[6]);
        currentWaypointIndex = 0; 
    }
    public void goToOilRefineryPumps(){
        graph.AStar(currentNode, wps[4]);
        currentWaypointIndex = 0; 
    }
    public void goToTankers(){
        graph.AStar(currentNode, wps[5]);
        currentWaypointIndex = 0; 
    }
    public void goToRadar(){
        graph.AStar(currentNode, wps[8]);
        currentWaypointIndex = 0; 
    }
    public void CommandPost(){
        graph.AStar(currentNode, wps[7]);
        currentWaypointIndex = 0; 
    }
    public void MiddleOfMap(){
        graph.AStar(currentNode, wps[2]);
        currentWaypointIndex = 0; 
    }
    
}
