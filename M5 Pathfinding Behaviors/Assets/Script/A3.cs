﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class A3 : MonoBehaviour
{
     NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;
    Vector3 wanderTarget;
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        
        playerMovement = target.GetComponent<WASDMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        //agent 3
        if(this == canSeeTarget()){
            wander();
        } else evade();
    }

     bool canSeeTarget(){ //can see if within line of sight with agent

        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
        
        if(Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo)){
            return raycastInfo.transform.gameObject.tag == "Player";
        }
        return false;
    }

    public void seek(Vector3 location) {
        agent.SetDestination(location);
    }

    public void wander(){ //wander around the area
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderTarget += new Vector3(Random.Range(-1f, 1f) * wanderJitter,
                                    0, 
                                    Random.Range(-1f, 1f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        seek(targetWorld);
    }

    public void evade(){ //predict then flee to another direction
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);
        flee(target.transform.position + target.transform.forward * lookAhead);
    }

    public void flee(Vector3 location){
         Vector3 fleeDirection = location - this.transform.position;
         agent.SetDestination(this.transform.position - fleeDirection);
    }
}
