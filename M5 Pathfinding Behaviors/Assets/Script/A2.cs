﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class A2 : MonoBehaviour
{
    NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;
    Vector3 wanderTarget;
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        
        playerMovement = target.GetComponent<WASDMovement>();
    }

    void Update()
    {
        //agnet 2
        if(this == canSeeTarget()){
            wander();
        } else {cleverHide();} 
        
    }

    bool canSeeTarget(){ //can see if within line of sight with agent

        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
        
        if(Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo)){
            return raycastInfo.transform.gameObject.tag == "Player";
        }
        return false;
    }
    public void seek(Vector3 location) {
        agent.SetDestination(location);
    }

    public void wander(){ //wander around the area
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderTarget += new Vector3(Random.Range(-1f, 1f) * wanderJitter,
                                    0, 
                                    Random.Range(-1f, 1f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        seek(targetWorld);
    }

     public void cleverHide(){

        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDir = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHidingSpots()[0]; //get first one 

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length; //hiding spot position

        for (int i = 0; i < hidingSpotsCount; i++){
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; //offset

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

            if(spotDistance < distance){ //choose nearest hiding spot
                chosenSpot = hidePosition;
                chosenDir = hideDirection; //raycast
                chosenGameObject = World.Instance.GetHidingSpots()[i];               
                distance = spotDistance;
            }
        }

        Collider hideCol = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDir.normalized);
        RaycastHit info;
        float rayDistance = 100f;
        hideCol.Raycast(back, out info, rayDistance);

        seek(info.point + chosenDir.normalized * 5); //go to chosen spot
    }

}
