﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour {

    public static AIControl instance;
    NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;

    //
    Vector3 wanderTarget;

    void Start() {
        instance = this;
        agent = this.GetComponent<NavMeshAgent>();
        
        playerMovement = target.GetComponent<WASDMovement>();

    }

    void Update() {
        //seek(target.transform.position); //seek
        //flee(target.transform.position); //flee
        //pursue(); //predixct
        //evade(); //predict then flee to another direction
        //wander(); //wander around the area
        //hide();
        //cleverHide();

        // if(canSeeTarget()){
        //     cleverHide();
        // }

        
        //agent 1
        // if(GameObject.FindGameObjectWithTag("A1") == canSeeTarget()){
        //     wander();
        // } else {pursue();}


        //agnet 2
        // if(GameObject.FindGameObjectWithTag("A2") == canSeeTarget()){
        //     wander();
        // } else {cleverHide();} 


        //agent 3
        // if(GameObject.FindGameObjectWithTag("A3") == canSeeTarget()){
        //     wander();
        // } else evade();


    }

    public void seek(Vector3 location) {
        agent.SetDestination(location);
    }

    public void flee(Vector3 location){
         Vector3 fleeDirection = location - this.transform.position;
         agent.SetDestination(this.transform.position - fleeDirection);
    }

    public void pursue(){ //predict
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);
        seek(target.transform.position + target.transform.forward * lookAhead);
    }

    public void evade(){ //predict then flee to another direction
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);
        flee(target.transform.position + target.transform.forward * lookAhead);
    }

    public void wander(){ //wander around the area
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderTarget += new Vector3(Random.Range(-1f, 1f) * wanderJitter,
                                    0, 
                                    Random.Range(-1f, 1f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        seek(targetWorld);
    }

    public void hide(){

        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length; //hiding spot position

        for (int i = 0; i < hidingSpotsCount; i++){
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; //offset

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

            if(spotDistance < distance){ //choose nearest hiding spot
                chosenSpot = hidePosition;
                distance = spotDistance;
            }
        }

        seek(chosenSpot); //go to chosen spot
    }

    public void cleverHide(){

        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDir = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHidingSpots()[0]; //get first one 

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length; //hiding spot position

        for (int i = 0; i < hidingSpotsCount; i++){
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; //offset

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

            if(spotDistance < distance){ //choose nearest hiding spot
                chosenSpot = hidePosition;
                chosenDir = hideDirection; //raycast
                chosenGameObject = World.Instance.GetHidingSpots()[i];               
                distance = spotDistance;
            }
        }

        Collider hideCol = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDir.normalized);
        RaycastHit info;
        float rayDistance = 100f;
        hideCol.Raycast(back, out info, rayDistance);

        seek(info.point + chosenDir.normalized * 5); //go to chosen spot
    }

   public bool canSeeTarget(){ //can see if within line of sight with agent

        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
        
        if(Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo)){
            return raycastInfo.transform.gameObject.tag == "Player";
        }
        return false;
    }
}
