﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    
    public float speed = 5;
    void Start() {
        
    }

    void Update() {
        transform.Translate(Input.GetAxis("Horizontal")*Time.deltaTime*speed,  //strafe
                            0f, Input.GetAxis("Vertical")*Time.deltaTime*speed);
        
    }
}
