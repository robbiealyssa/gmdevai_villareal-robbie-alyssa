﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
   
    public Transform player;
    public float petSpeed = 3;
    public float rotSpeed = 2;
    void Update() {
        Vector3 lookAtGoal = new Vector3(player.position.x, 
                                        this.transform.position.y, 
                                        player.position.z);

        transform.LookAt(lookAtGoal);
        Vector3 direction = lookAtGoal - transform.position;

        // slowly rotates characters orientation towards our direction
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                    Quaternion.LookRotation(direction), 
                                                    Time.deltaTime*rotSpeed); 
        //

        if(Vector3.Distance(lookAtGoal, transform.position) > 1){
            transform.Translate(0,0,petSpeed*Time.deltaTime); // moving forward with constant change
        }

    }
}
