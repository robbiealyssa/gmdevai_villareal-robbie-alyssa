﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carMovement : MonoBehaviour {
    
     public float speed = 0;
    public float rotationSpeed = 3;
    public float acceleration = 5;
    public float deceleration = 5;
    public float minSpeed = 0;
    public float maxSpeed = 10;
    public float breakAngle = 20;
    public Transform goal;

    void LateUpdate() {
        Vector3 lookAtGoal = new Vector3(goal.transform.position.x,
                                            this.transform.position.y,
                                            goal.transform.position.z);
        Vector3 direction = lookAtGoal - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotationSpeed);

        //speed = Mathf.Clamp((speed+acceleratio*Time.deltaTime), minSpeed, maxSpeed);
        if(Vector3.Angle(goal.forward, this.transform.forward) > breakAngle && speed > 2){
            speed = Mathf.Clamp(speed - (deceleration * Time.deltaTime), minSpeed, maxSpeed);
        }else {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
        }

        this.transform.Translate(0,0,speed);
    }
}
