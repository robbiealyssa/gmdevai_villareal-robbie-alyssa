﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waypointFollow1 : MonoBehaviour
{
    public float speed = 5;
    public float rotationSpeed = 3;
    public float accuracy = 1;
    public GameObject[] waypoints;
    int currentWaypointIndex = 0;
    GameObject currentWaypoint;

    void Start() {
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    
    void LateUpdate() {
        if(waypoints.Length == 0)return; // not waypoints terminate the action

        currentWaypoint = waypoints[currentWaypointIndex];
        Vector3 lookAtGoal = new Vector3(currentWaypoint.transform.position.x,
                                            this.transform.position.y,
                                            currentWaypoint.transform.position.z);
        Vector3 direction = lookAtGoal - this.transform.position;

        if(direction.magnitude < 1f){
            currentWaypointIndex++; // next waypoint

            //reset once its already in the last waypoint, back to the first waypoint
            if(currentWaypointIndex >= waypoints.Length){
                currentWaypointIndex = 0;
            }

        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotationSpeed);

        this.transform.Translate(0,0,speed * Time.deltaTime);
    }
}
