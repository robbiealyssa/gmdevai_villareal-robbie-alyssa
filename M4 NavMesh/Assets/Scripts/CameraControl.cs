﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
   Vector2 mouseLook;
   Vector2 smoothMovement;
   public float sensitivity = 2;
   public float smoothing = 2; 
   GameObject player;

    void Start() {
        player = this.transform.parent.gameObject;
    }

    void Update() {
        var mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity*smoothing, sensitivity*smoothing));
        
        smoothMovement.x = Mathf.Lerp(smoothMovement.x, mouseDelta.x, 1/smoothing); //moves smoothly
        smoothMovement.y = Mathf.Lerp(smoothMovement.y, mouseDelta.y, 1/smoothing);
        mouseLook += smoothMovement;

        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, player.transform.up);
    }
}
