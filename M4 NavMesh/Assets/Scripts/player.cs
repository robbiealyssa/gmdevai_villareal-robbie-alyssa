﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {

    [SerializeField] public float speed = 5;
    public static player instance;


    void Start() {
        instance = this;
    }

    void Update() {
         transform.Translate(Input.GetAxis("Horizontal")*Time.deltaTime*speed,
                            0f, Input.GetAxis("Vertical")*Time.deltaTime*speed);
    }
}
